const express = require('express')
const router = express.Router()
const TaskController = require('../controllers/TaskController')

// Create single task
router.post('/create',(request, response) => {
	TaskController.createTask(request.body).then((result) => {
		response.send(result)
	})
})


// Get all tasks 
router.get('/',(request, response) => {
	TaskController.getAllTasks().then((result) => {
		response.send(result)
	})
})

// Update a task

/* /:id is a url parameter  an alias; note that the :id is just an alias, it is changeable*/

router.patch('/:id/update', (request,response) => {
	TaskController.updateTask(request.params.id, request.body).then((result) => {
		response.send(result)
	})
})


// Delete Task 
/*
	 MINI ACTIVITY 

	 1. Setup a route for deleting a task 
	 2. Setup the controller for deleting a task 
	 3. Build the controller function responsible for deleting a task 
	 4. Send the result of that controller function as a response

	 Endpoint: '/:id/delete'

	 NOTE : yung upated task na  module, palitan ung .save ng .delete, it also wor

*/
router.delete('/:id/delete', (request,response) => {
	TaskController.deleteTask(request.params.id).then((result) => {
		response.send(result)
	})
})

// no need for other data because you will just delete 


// ACTIVITY: 

//ROUTE - getting a specific task 


router.get('/:id/', (request, response) => {
	TaskController.specificTask(request.params.id).then((result) => {
		response.send(result)
	})
})




// ROUTE - change status to complete 

router.patch('/:id/complete', (request, response) => {
	TaskController.completeStatus(request.params.id, request.body).then((result) => {
		response.send(result)
	})
})


module.exports = router



/*
MVC 
 Models
 View
 Controllers
*/