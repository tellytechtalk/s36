const Task = require('../models/Task.js') 
//.. dahil kelangan muna natin lumabas mula sa controller folder

module.exports.createTask = (data) => {
	let newTask = new Task({
		name:data.name
	})
//note: createTask is just a function name that you can assign 


	return newTask.save().then((savedTask,error) =>{
		if(error){
			console.log(error)
			return error
		}

		return savedTask
	})
}

module.exports.getAllTasks = () => {
	return Task.find({}).then((result) => {
		return result
	})
}

module.exports.updateTask = (task_id, new_data) => {
	return Task.findById(task_id).then((result, error) => {
		if(error){
			console.log(error)
			return error
		}

		result.name = new_data.name

		return result.save().then((updatedTask, error) => {
			if(error){
				console.log(error)
				return error
			}

			return updatedTask
		})

	})
}


module.exports.deleteTask = (task_id) => {
    return Task.deleteOne({ _id: task_id }).then((deletedTask, error) =>{
    	if(error){
    		console.log(error)
    		return error
    	}
    	return deletedTask
    })
}





// ACTIVITY: 
// CONTROLLER - for a  Specific Task

module.exports.specificTask = (task_id) => {
	return Task.findById(task_id).then((foundSpecificTask, error) => {
		if(error) {
			console.log(error)
			return error
		}

		return foundSpecificTask

	})
}

//  CONTROLLER - change status to complete

module.exports.completeStatus = (task_id, new_data_status) => {
	return Task.findById(task_id).then((result, error) => {
		if(error) {
			console.log(error)
			return error
		}

		result.status = new_data_status.status

		return result.save().then((completedStatus, error) => {
			if(error) {
			console.log(error)
			return error
		}

			return completedStatus

		})
	})
}
